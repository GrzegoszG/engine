﻿using Assets.Scripts.Extensions;
using Pathfinding;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class Mover
    {
        private AIDestinationSetter _aids;
        private AIPath _path;

        private Vector3 _lastPosition = Vector3.zero;
        private MoveMode _mode;
        public Transform Target;
        private NPC _npc;

        private float timeStuck;

        public float TimeStuck
        {
            get { return timeStuck; }
            private set { timeStuck = value; }
        }

        public Mover(NPC npc)
        {
            _npc = npc;
            _aids = _npc.GetComponent<AIDestinationSetter>();
            _path = _npc.GetComponent<AIPath>();
        }

        public void Tick()
        {
            if(_mode == MoveMode.FOLLOW)
            {
                if (RemainingDistance() < _npc.CurioInteractionDistance)
                    _npc.Target.gameObject.SetActive(false);
            }
        }

        public float RemainingDistance()
        {
            var dist = Vector3.Distance(_aids.target.position, _npc.transform.position);
            return Mathf.Abs(dist);
        }

        public void SetTarget(Transform transform, MoveMode moveMode)
        {
            if (transform == null)
                return;

            _mode = moveMode;

            if(moveMode == MoveMode.FOLLOW)
            {
                RunTowards(transform);
            }
            else if(moveMode == MoveMode.FLEE)
            {
                RunFrom(transform);
            }
            OnEnter();
        }

        public void RunTowards(Transform target)
        {
            _aids.target = target;
            _path.maxSpeed = _npc.Speed;
        }

        public void RunFrom(Transform other)
        {
            var vec = GetFleeTarget(other);
            _npc.Target.position = vec;
            _aids.target = _npc.Target;
            _path.maxSpeed = _npc.FleeSpeed;
        }

        public Vector2 GetFleeTarget(Transform other)
        {
            Vector2 direction = (_npc.transform.position - other.position).normalized;
            var angle = Random.Range(-_npc.FleeMaxAngle, _npc.FleeMaxAngle);
            var distance = Random.Range(_npc.MinFleeDistance, _npc.MaxFleeDistance);
            var vec = direction.Rotate(angle) * distance;
            return vec;
        }

        private void OnEnter()
        {
            TimeStuck = 0f;
            _path.enabled = true;
            _aids.enabled = true;
            _npc.Target.gameObject.SetActive(true);
        }

        public void OnExit()
        {
            _path.enabled = false;
            _aids.enabled = false;
            _npc.Target.gameObject.SetActive(false);
        }
    }

    public enum MoveMode
    {
        NONE,
        FOLLOW,
        FLEE
    }
}
