using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine;

public class DefaultDetector : MonoBehaviour, IDetector
{
    [SerializeField]
    private Collider2D _collider;

    [SerializeField]
    public List<Actor> Detected { get; set; }

    void Start()
    {
        _collider = GetComponent<Collider2D>();
        _collider.isTrigger = true;
        Detected = new List<Actor>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("+ "+collision.name);
        if(collision.gameObject.TryGetComponent<Actor>(out var actor))
        {
            OnDetect(actor);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("- "+collision.name);
        if (collision.gameObject.TryGetComponent<Actor>(out var actor))
        {
            OnLose(actor);
        }
    }

    public void OnDetect(Actor actor)
    {
        Detected.Add(actor);
        Debug.Log("Detected: " + actor.name);
    }

    public void OnLose(Actor actor)
    {
        Detected.Remove(actor);
        Debug.Log("Lost: " + actor.name);
    }
}
