﻿namespace Assets.Scripts
{
    public interface IInteractor
    {
        string InteractorName { get; }
        Identity Identity { get; }
        void InteractWith(IInteractable interactable);
    }
}
