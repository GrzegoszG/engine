using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Templates
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Curio", order = 1)]
    public class CurioTemplate : ScriptableObject
    {
        public Sprite Sprite;
    }
}