﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Templates
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/NPC", order = 1)]
    public class NPCTemplate : ScriptableObject
    {
        public string Name;
        public float Speed;
    }
}
