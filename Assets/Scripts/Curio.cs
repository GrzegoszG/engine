﻿using UnityEngine;

namespace Assets.Scripts.Entities
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class Curio : MonoBehaviour, IInteractable
    {
        [SerializeField]
        private ParticleSystem _particleSystem;

        [SerializeField]
        private Sprite _sprite;

        private bool _readyToUse;

        private void Start()
        {
            _readyToUse = true;
        }

        public bool ReadyToUse
        {
            get { return _readyToUse; }
            set { _readyToUse = value; }
        }


        public void Interact(IInteractor interactor)
        {
            //var message = $"{interactor.Identity.GetSelf().name} is interacting with {gameObject.name}!";
            var message = $"{interactor.InteractorName} is interacting with {gameObject.name}!";
            Debug.Log(message);
            _particleSystem?.Play();
        }
    }
}
