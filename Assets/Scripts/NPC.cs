using Assets.Scripts.States;
using Assets.Scripts.Templates;
using Pathfinding;
using System;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

namespace Assets.Scripts
{
    [RequireComponent(typeof(AIDestinationSetter))]
    [RequireComponent(typeof(AIPath))]
    public class NPC : Actor
    {
        [SerializeField]
        private bool angry;

        [SerializeField]
        private bool hasNothingToDo;

        public Transform Target;
        public NPCTemplate Template;

        public Mover Mover;

        private StateMachine _stateMachine;

        public float FleeDistance;
        public float FleeSpeedMult;
        public float FleeSpeed => Speed * FleeSpeedMult;
        public float FleeMaxAngle;

        public float MinFleeDistance;
        public float MaxFleeDistance;
        public float CurioInteractionDistance;

        public IInteractable Interactable;

        void Awake()
        {
            InitTemplate();
            _stateMachine = new StateMachine();
            Mover = new Mover(this);
            SetupStateMachine();   
        }

        private void InitTemplate()
        {
            this.Name = Template.Name;
            this.Speed = Template.Speed;
        }

        private void SetupStateMachine()
        {
            var idle = new Idle();
            var attack = new Attack();
            var move = new MoveToTarget(this);
            var flee = new Flee(this);
            var curioSearch = new SearchForCurio(this);
            var use = new UseCurio(this);
            //var rutine = new Rutine(this);

            _stateMachine.AddAnyTransition(flee, flee.IsScaredCondition());
            _stateMachine.AddAnyTransition(attack, IsAngry());
            
            _stateMachine.AddTransition(curioSearch, move, HasInteractionTarget());
            _stateMachine.AddTransition(move, use, HasInteractionTarget());
            _stateMachine.AddAnyTransition(move, HasTarget());
            _stateMachine.AddAnyTransition(curioSearch, HasNoInteractionTarget());
            //_stateMachine.AddAnyTransition(idle, HasNothingToDo());

            _stateMachine.SetState(curioSearch);
        }

        private void OnDrawGizmos()
        {
            var color = Gizmos.color;
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, FleeDistance);
            Gizmos.color = color;
        }

        void Update()
        {
            _stateMachine?.Tick();
        }

        public Func<bool> IsAngry() => () => angry;

        public Func<bool> HasNothingToDo() => () => hasNothingToDo;

        public Func<bool> HasTarget() => () => Target.gameObject.activeInHierarchy;

        public Func<bool> HasInteractionTarget() => () => Interactable != null;
        public Func<bool> HasNoInteractionTarget() => () => Interactable == null;
    }
}
