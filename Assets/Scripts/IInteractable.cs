﻿namespace Assets.Scripts
{
    public interface IInteractable
    {
        void Interact(IInteractor interactor);
    }
}
