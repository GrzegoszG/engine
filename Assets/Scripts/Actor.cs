using Assets.Scripts;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public abstract class Actor : MonoBehaviour, IInteractable, IInteractor
{
    public string Name { get; internal set; }
    public float Speed { get; internal set; }

    private Rigidbody2D rb;

    private Identity _identity;
    public Identity Identity
    {
        get
        {
            return _identity;
        }
    }

    public string InteractorName { get => gameObject.name; }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
    }

    public void InteractWith(IInteractable interactable)
    {
        interactable.Interact(this);
    }

    public void Interact(IInteractor interactor)
    {
        
    }
}
