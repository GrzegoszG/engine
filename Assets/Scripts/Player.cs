using UnityEngine;

public class Player : Actor
{
    private StateMachine _stateMachine;

    void Start()
    {
        Application.targetFrameRate = 30;
    }

    private void Update()
    {
        Movement();
    }

    private KeyCode up = KeyCode.W;
    private KeyCode down = KeyCode.S;
    private KeyCode left = KeyCode.A;
    private KeyCode right = KeyCode.D;

    private float horizontalInput;
    private float vertivcalInput;

    public float Speed;

    private void Movement()
    {
        direction = Vector3.zero;

        if (Input.GetKey(up))
        {
            ++direction.y;
        }
        if (Input.GetKey(down))
        {
            --direction.y;
        }
        if(Input.GetKey(left))
        {
            --direction.x;
        }
        if(Input.GetKey(right))
        {
            ++direction.x;
        }

        transform.Translate(direction.normalized * Speed * Time.deltaTime);
    }

    private Vector3 direction = Vector3.zero;
}
