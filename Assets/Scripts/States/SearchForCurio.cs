﻿using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class SearchForCurio : IState
    {
        private NPC _npc;
        private Curio _curio;

        public SearchForCurio(NPC npc)
        {
            _npc = npc;
        }

        public void OnEnter()
        {

        }

        public void OnExit()
        {
            _npc.Target.position = _curio.transform.position;
            _npc.Target.gameObject.SetActive(true);
        }

        public void Tick()
        {
            _curio = FindOtherNearestCurio();
            _npc.Interactable = _curio;
        }

        private Curio FindOtherNearestCurio(int pickFromNearest = 3)
        {
            var curio = UnityEngine.Object.FindObjectsOfType<Curio>()
             .OrderBy(t => Vector3.Distance(_npc.transform.position, t.transform.position))
             .Where(t => t.ReadyToUse)
             .Take(pickFromNearest)
             .OrderBy(t => UnityEngine.Random.Range(0, int.MaxValue))
             .FirstOrDefault();

            return curio;
        }
    }
}
