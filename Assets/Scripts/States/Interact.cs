﻿using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.States
{
    public class Interact : IState
    {
        private Curio _target;
        private IInteractor _interactor;

        public void SetTarget(Curio curio)
        {
            _target = curio;
        }

        public Interact(IInteractor interactor)
        {
            _interactor = interactor;
        }

        public void OnEnter()
        {
          
        }

        public void OnExit()
        {
           
        }

        public void Tick()
        {
           if(_target.ReadyToUse)
           {
                _target.Interact(_interactor);
           }
        }
    }
}
