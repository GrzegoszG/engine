﻿using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.States
{
    public class UseCurio : IState
    {
        private NPC _npc;

        public UseCurio(NPC npc)
        {
            _npc = npc;
        }

        public void OnEnter()
        {

        }

        public void OnExit()
        {
            _npc.Interactable = null;
        }

        public void Tick()
        {
            if(_npc.Interactable is Curio)
            {
                Use((Curio)_npc.Interactable);
            }
            _npc.Interactable = null;
        }

        private void Use(Curio curio)
        {
            if (curio != null && curio.enabled && curio.isActiveAndEnabled && curio.ReadyToUse)
                _npc.InteractWith(curio);
        }
    }
}
