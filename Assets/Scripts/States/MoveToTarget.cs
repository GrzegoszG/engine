﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.AI;
using Pathfinding;

internal class MoveToTarget : IState
{
    private readonly NPC _npc;
    
    public MoveToTarget(NPC npc)
    {
        _npc = npc;
    }
    
    public void Tick()
    {
        _npc.Mover.Tick();
    }

    public void OnEnter()
    {
        _npc.Mover.SetTarget(_npc.Target, MoveMode.FOLLOW);
    }

    public void OnExit()
    {
        _npc.Mover.OnExit();
    }
}